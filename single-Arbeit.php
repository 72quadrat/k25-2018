<?php
/**
 * Einzelansichten der Arbeiten gibt es nicht
 *
 */

//get_header(); // This fxn gets the header.php file and renders it

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
    if( has_term( 'corporate', 'filter' ) ) {
      header("Location: http://".$_SERVER['HTTP_HOST']."?filter=corporate#".get_the_ID(), true, 301);
      die('hello');
    } else {
      header("Location: http://".$_SERVER['HTTP_HOST']."?filter=editorial#".get_the_ID(), true, 301);
      die('nope');
    }
	} // end while
} // end if

exit();
?>
