<?php
// Funktion zum reparieren der Texte für photoswip

// Prüfen welche Art von Beitrags-Inhalt wird haben.
// ---------------------------------------------------------
// EINE GALERIE
if ( get_post_gallery() ) {
  $gallery = get_post_gallery( get_the_ID(), false );
  $images = array_flip(explode( ',', $gallery['ids'] ));
  foreach ($images as $id => $value) {
     $meta = wp_get_attachment_metadata( $id );
     $content_post = get_post($id);
     $content = $content_post->post_content;
     $sizes = array();
     foreach($meta['sizes'] as $sizename => $size){
      $src = wp_get_attachment_image_src( $id, $sizename );
      $test ='nix';
      $sizes[$sizename] = array(
        'src' => $src[0],
        'width' => $size['width'],
        'height' => $size['height']
      );
    }
    $images[$id] = array(
      'sizes' => $sizes,
      'meta' => $meta['image_meta'],
      'title' => $content,
    );
  }
  $anzahl = count($images);
  if ($anzahl > 1) {
    $anzahl_text = count($images).' Bilder';
  } else {
    $anzahl_text = '1 Bild';
  }
};
// ---------------------------------------------------------
// EIN VIDEO
$content = trim(get_the_content());
if ( preg_match("/https:\/\/youtube.com\//", $content) OR preg_match("/https:\/\/vimeo.com\//", $content) ) {
  $video = $content;
  preg_match_all('!\d+!', $video, $video_id);
  $video_id = $video_id[0][0];
}
?>

<!-- // Teaser ausgeben START -->
<button data-photoswipeitems="photoswipeitems<?php the_ID() ?>" data-gid="<?php the_ID() ?>">
  <?php edit_post_link('bearbeiten', '', ''); ?>
  <!-- Das Vorschaubild -->
  <?php if ( isset($gallery) ) {
    // Das erste Bild aus der Galerie ausgeben
    $gallery = explode( ',', $gallery['ids'] );
    $first_image_srcset = wp_get_attachment_image_srcset($gallery[0]);
    $first_image_src = wp_get_attachment_image_src($gallery[0], 'medium');
    echo '<img src="'.$first_image_src[0].'" srcset="'.$first_image_srcset.'" width="'.$first_image_src[1].'" height="'.$first_image_src[2].'" alt="'.get_the_title().'">';
  } elseif (isset($video_id)) {
    // Bei Videos den Play-Button ausgeben
    echo '<div class="playbutton"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-3 17v-10l9 5.146-9 4.854z"/></svg></div>';
    the_post_thumbnail('medium'); //Get the thumbnail to this post.
  } else {
    // Bei allen anderen Beiträgen nur das Thumbnail ausgeben
    the_post_thumbnail('medium'); //Get the thumbnail to this post.
  }
  ?>

  <!-- Der Infotext -->
  <div class="content">
    <div class="text">
      <h5><?php the_title()?></h5>
      <p>
        <?php if (has_excerpt()) { echo get_the_excerpt().'<br/>'; }?>
        von <?php the_author_meta('first_name') ?> <?php the_author_meta('last_name') ?>
      </p>
      <!--<p><?php echo $anzahl_text ?></p>-->
    </div>
  </div>

  <?php if (isset($anzahl)) : ?>
    <?php if ($anzahl > 1) : ?>
      <!-- Es gibt mehr als ein Bild also einen Counter ausgeben -->
      <span class="multi-images"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><polyline fill="none" stroke="#ffffff" stroke-miterlimit="10" points="1.5,12 1.5,1.5 15,1.5 "/>
      <polyline fill="none" stroke="#ffffff" stroke-miterlimit="10" points="3.5,14 3.5,3.5 17,3.5 "/>
      <rect x="5.5" y="5.5" fill="none" stroke="#ffffff" stroke-miterlimit="10" width="14" height="10"/></svg></span>
    <?php endif ?>
  <?php endif ?>

</button>
<!--
<?php if ( isset($gallery) ) : ?>
  <div class="gallery-thumbnails">
      <?php
      foreach($gallery as $gallery_image){
        $first_image_srcset = wp_get_attachment_image_srcset($gallery_image);
        $first_image_src = wp_get_attachment_image_src($gallery_image, 'thumbnail');
        echo '<img src="'.$first_image_src[0].'" srcset="'.$first_image_srcset.'" width="'.$first_image_src[1].'" height="'.$first_image_src[2].'" alt="'.get_the_title().'">';
      };?>
  </div>
<?php endif ?>
-->
<!-- // Teaser ausgeben ENDE -->

<!-- // Photoswipe Javascript Variable befüllen START -->
<script>
var photoswipeitems<?php the_ID() ?> = [
<?php if ( isset($gallery) ) : ?>
  <?php
    $post   = get_post();
    $output =  strip_shortcodes($post->post_content );
    $output = str_replace('"', '\"', trim(preg_replace('/\s+/', ' ', $output)));
    if( strlen($output) > 20 ) {
      echo "{
          html: '<div class=\"pswp-post-wrap\"><div class=\"pswp-post\">".$output."</div></div>'
        },";
    }
  ?>
  <?php foreach ($images as $id => $image) : ?>
  {
    msrc:'<?php echo $image['sizes']['medium']['src'] ?>',
    src:'<?php echo $image['sizes']['large']['src'] ?>',
    w:'<?php echo $image['sizes']['large']['width'] ?>',
    h:'<?php echo $image['sizes']['large']['height'] ?>',
    title:'<?php echo $image['title'] ?>',

    <?php foreach ($image['sizes'] as $sizes_name => $image_size) : ?>

    <?php echo $sizes_name ?>: {
      src:'<?php echo $image_size['src']; ?>',
      w:'<?php echo $image_size['width']; ?>',
      h:'<?php echo $image_size['height']; ?>'
    },
    <?php endforeach; ?>

  },
  <?php endforeach; ?>
<?php elseif (isset($video)) : ?>
    {
    html:'<div class="video"><iframe src="https://player.vimeo.com/video/'+<?php echo $video_id ?>+'?autoplay=1&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>'
    },
<?php else : ?>
<?php $thumbnail_meta = wp_get_attachment_metadata(get_post_thumbnail_id()); ?>
    {
    msrc:'<?php echo get_the_post_thumbnail_url(get_the_id(), 'medium') ?>',
    src:'<?php echo get_the_post_thumbnail_url(get_the_id(), 'large') ?>',
    w:'<?php echo $thumbnail_meta['sizes']['large']['width'] ?>',
    h:'<?php echo $thumbnail_meta['sizes']['large']['height'] ?>',
    title:'<?php echo str_replace('"', '\"', esc_html__(trim(preg_replace('/\s+/', ' ', $thumbnail_meta['image_meta']['caption']))))?>'
    },
<?php //print_r($thumbnail_meta) ?>
<?php endif ?>
];
</script>
<!-- // Photoswipe Javascript Variable befüllen ENDE -->
