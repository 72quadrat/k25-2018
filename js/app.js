jQuery(document).foundation();

jQuery(function() {

  ///////////////////////////////////////////
  // Responsive Video
  jQuery(".the-content").fitVids();

  ///////////////////////////////////////////
  // Menu

  // Menu schließen wenn man auf einen Link geklickt hat
  jQuery('#offCanvas .menu a').click(function() {
    jQuery('#offCanvas').foundation('close');
  });

  ///////////////////////////////////////////
  // Isotope
  var $grid = jQuery('.images-content').isotope({
    itemSelector: '.hentry'
  });
  var $gridcontainer = jQuery('.images-content');
  jQuery('.images-content').addClass('loaded');
  // layout Isotope after each image loads
  $gridcontainer.imagesLoaded().progress( function(instance, image ) {
    $grid.isotope('layout');
    console.log(instance);
  });
  // Isotope wenn alle Bilder geladen sind
  $gridcontainer.imagesLoaded().always( function() {
    $grid.isotope('layout');
  });

  ///////////////////////////////////////////
  // Filter

  // Ein und ausblenden der Funktion
  jQuery('.filter_toggler').click(function(e) {
    //e.preventDefault();
    jQuery(this).toggleClass('active');
    jQuery(this).parent().parent().find('.filter-funktion').slideToggle();
  });

  // Die eigentliche Filterfunktion
  var filters = {};
  jQuery('.filter').on( 'click', 'a', function(e) {
    e.preventDefault();
    var $this = jQuery(this);
    $this.parent().parent().find('li.is-active').removeClass('is-active');
    $this.parent().addClass('is-active');
    // get group key
    var $buttonGroup = $this.parents('.button-group');
    var filterGroup = $buttonGroup.attr('data-filter-group');
    // set filter for group

    filters[ filterGroup ] = $this.attr('data-filter');
    // combine filters
    var filterValue = concatValues( filters );
    $grid.isotope({ filter: filterValue });
  });
  // flatten object by concatting values
  function concatValues( obj ) {
    var value = '';
    for ( var prop in obj ) {
      value += obj[ prop ];
    }
    return value;
  }

  ///////////////////////////////////////////
  // Photoswipe
  var pswpElement = document.querySelectorAll('.pswp')[0];
  jQuery( ".hentry button" ).click(function() {
    var photoswipeitems = jQuery(this).attr('data-photoswipeitems');
    var gid = jQuery(this).attr('data-gid');
    var elementwidth = jQuery(this).width();
    var pageYScroll = window.pageYOffset || document.documentElement.scrollTop;
    var elementtop = jQuery(this).offset().top - pageYScroll;
    var elementleft = jQuery(this).offset().left;
    var options = {
      index: 0,
      fullscreenEl: false,
      shareEl: false,
      zoomEl: false,
      maxSpreadZoom: 1,
      bgOpacity: 0.95,
      closeOnScroll: false,
      galleryUID: gid,
      getThumbBoundsFn: function(index) {
          return {x:elementleft, y:elementtop + pageYScroll, w:elementwidth};
      }
    };
    var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, eval(photoswipeitems), options);

    // Event wenn die Galerie komplett geschlossen wurd
    gallery.listen('destroy', function() {
      // Video entfernen wenn die Galerie geschlossen wird
      jQuery('.pswp .video').remove();
    });

    // Event während die Schließen-Animation läuft
    gallery.listen('close', function() {
      // Video und Bilder ausblenden wenn die Galerie geschlossen wird
      jQuery('.pswp img, .pswp .video').fadeOut();
    });

    // create variable that will store real size of viewport
    var realViewportWidth,
        useLargeImages = false,
        firstResize = true,
        use_size = 's',
        imageSrcWillChange;

    // beforeResize event fires each time size of gallery viewport updates
    gallery.listen('beforeResize', function() {
        // gallery.viewportSize.x - width of PhotoSwipe viewport
        // gallery.viewportSize.y - height of PhotoSwipe viewport
        // window.devicePixelRatio - ratio between physical pixels and device independent pixels (Number)
        //                          1 (regular display), 2 (@2x, retina) ...


        // calculate real pixels when size changes
        realViewportWidth = gallery.viewportSize.x * window.devicePixelRatio;
        realViewportWidth = gallery.viewportSize.x;

        // Code below is needed if you want image to switch dynamically on window.resize

        if(use_size != 's' && realViewportWidth < 640) {
            use_size    = 's';
            imageSrcWillChange = true;
        } else if(use_size != 'm' && realViewportWidth >= 640 && realViewportWidth < 960){
            use_size    = 'm';
            imageSrcWillChange = true;
        } else if(use_size != 'l' && realViewportWidth >= 960 && realViewportWidth < 1280){
            use_size    = 'l';
            imageSrcWillChange = true;
        } else if(use_size != 'xl' && realViewportWidth >= 1280 && realViewportWidth < 1600){
            use_size    = 'xl';
            imageSrcWillChange = true;
        } else if(use_size != 'xxl' && realViewportWidth >= 1600 && realViewportWidth < 1920){
            use_size    = 'xxl';
            imageSrcWillChange = true;
        } else if(use_size != 'xxxl' && realViewportWidth >= 1920){
            use_size    = 'xxxl';
            imageSrcWillChange = true;
        }
        // console.log('Zu benutzende Groesse ist: '+use_size);
        // console.log('Viewport: '+realViewportWidth);

        // Invalidate items only when source is changed and when it's not the first update
        if(imageSrcWillChange && !firstResize) {
            // invalidateCurrItems sets a flag on slides that are in DOM,
            // which will force update of content (image) on window.resize.
            gallery.invalidateCurrItems();
        }

        if(firstResize) {
            firstResize = false;
        }

        imageSrcWillChange = false;

    });

    // gettingData event fires each time PhotoSwipe retrieves image source & size
    gallery.listen('gettingData', function(index, item) {

        // Prüfen ob die gewünsche Größe überhaupt existiert
        if(use_size == 'xxxl' && item.xxxl == undefined) { use_size = 'xxl'; }
        if(use_size == 'xxl' && item.xxl == undefined) { use_size = 'xl'; }
        if(use_size == 'xl' && item.xl == undefined) { use_size = 'l'; }
        if(use_size == 'l' && item.l == undefined) { use_size = 'm'; }
        if(use_size == 'm' && item.m == undefined) { use_size = 's'; }
        if(use_size == 's' && item.s == undefined) { use_size = 'error'; }

        // Set image source & size based on real viewport width
        if( use_size == 's' ) {
            item.src = item.s.src;
            item.w = item.s.w;
            item.h = item.s.h;
        } else if (use_size == 'm') {
            item.src = item.m.src;
            item.w = item.m.w;
            item.h = item.m.h;
        } else if (use_size == 'l') {
            item.src = item.l.src;
            item.w = item.l.w;
            item.h = item.l.h;
        } else if (use_size == 'xl') {
           item.src = item.xl.src;
            item.w = item.xl.w;
            item.h = item.xl.h;
        } else if (use_size == 'xxl') {
            item.src = item.xxl.src;
            item.w = item.xxl.w;
            item.h = item.xxl.h;
        } else if (use_size == 'xxxl') {
            item.src = item.xxxl.src;
            item.w = item.xxxl.w;
            item.h = item.xxxl.h;
        } else {
            item.src = item.src;
            item.w = item.w;
            item.h = item.h;
        }

        // It doesn't really matter what will you do here,
        // as long as item.src, item.w and item.h have valid values.
        //
        // Just avoid http requests in this listener, as it fires quite often

    });

    gallery.init();


  });
  ///////////////////////////////////////////
  // SmoothScroll
  // Smoth Scroll Desktop
  // var scrolldesktop = new Foundation.SmoothScroll(jQuery('.header a[href*="#"]'), {offset: 50});
  // // Smoth Scroll Mobil
  // var scrollmobile = new Foundation.SmoothScroll(jQuery('#offCanvas a[href*="#"]'), {offset: -30});
  ///////////////////////////////////////////
  // Hochscroll-Link
  jQuery('.scrollup').click(function() {
    jQuery('html,body').animate({ scrollTop: 0 }, 'slow');
    return false;
  });

  ///////////////////////////////////////////
  // Magellan
  var magellandesktop = new Foundation.Magellan(jQuery('.hauptmenu-desktop'), {offset: 20});
  var magellandesktop = new Foundation.Magellan(jQuery('#offCanvas a[href*="#"]'), {offset: -30});

});
