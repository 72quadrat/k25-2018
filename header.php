<?php
	/*-----------------------------------------------------------------------------------*/
	/* This template will be called by all other template files to begin
	/* rendering the page and display the header/nav
	/*-----------------------------------------------------------------------------------*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title>
	<?php bloginfo('name'); // show the blog name, from settings ?> |
	<?php is_front_page() ? bloginfo('description') : wp_title(''); // if we're on the home page, show the description, from the site's settings - otherwise, show the title of the post or page ?>
</title>

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php // We are loading our theme directory style.css by queuing scripts in our functions.php file,
	// so if you want to load other stylesheets,
	// I would load them with an @import call in your style.css
?>

<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->

<?php wp_head();
// This fxn allows plugins, and Wordpress itself, to insert themselves/scripts/css/files
// (right here) into the head of your website.
// Removing this fxn call will disable all kinds of plugins and Wordpress default insertions.
// Move it if you like, but I would keep it around.
?>

</head>

<body
	<?php
	$rand = 'zufallsfarbe-'.rand(1,4);
	body_class($rand);
	// This will display a class specific to whatever is being loaded by Wordpress
	// i.e. on a home page, it will return [class="home"]
	// on a single post, it will return [class="single postid-{ID}"]
	// and the list goes on. Look it up if you want more.
	?>
>

<div class="off-canvas position-right" id="offCanvas" data-off-canvas>

	<!-- Close button -->
	<button class="close-button" aria-label="Close menu" type="button" data-close>
		<span aria-hidden="true">&times;</span>
	</button>

	<!-- Menu -->
	<?php wp_nav_menu( array( 'theme_location' => 'mainmenu', 'menu_class' => 'menu vertical' ) ); // Display the user-defined menu in Appearance > Menus ?>

</div>



<div class="off-canvas-content" data-off-canvas-content><!-- off-canvas-content START -->

<header id="masthead" class="site-header" data-sticky-container>
	<div class="header sticky" data-sticky data-margin-top="0">
			<?php if (has_term( 'corporate', 'filter')) : ?>
				<a class="logolink" href="http://corporate.kollektiv25.com/" title="K25 Corporate" rel="home">
					<div class="logo-corporate">
						<svg version="1.1" baseProfile="basic" class="corporatelogo"
							 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="38.883px"
							 height="17.665px" viewBox="0 0 38.883 17.665" xml:space="preserve">
						<g>
							<path fill-rule="evenodd" clip-rule="evenodd" d="M25.673,17.665h-11.27V7.482h5.647c1.012,0,1.837-0.822,1.837-1.835
								c0-1.012-0.825-1.836-1.837-1.836h-5.652V0h5.652c3.112,0,5.646,2.533,5.646,5.647c0,3.113-2.533,5.646-5.646,5.646h-1.838v2.561
								h7.46V17.665"/>
							<path fill-rule="evenodd" clip-rule="evenodd" d="M33.237,17.665h-5.592v-3.811h5.592c1.013,0,1.837-0.823,1.837-1.836
								c0-1.013-0.824-1.835-1.837-1.835h-5.646V0h10.211v3.811h-6.4v2.562h1.836c3.113,0,5.646,2.532,5.646,5.645
								C38.883,15.132,36.351,17.665,33.237,17.665"/>
						</g>
						<polygon points="8.396,0 5.529,7.425 3.48,7.681 3.48,0 0,0 0,17.665 3.48,17.665 3.48,10.88 5.58,10.625
							8.498,17.665 12.569,17.665 8.679,8.781 12.466,0 "/>
						</svg>
						Corporate
					</div>
				</a>
			<?php else : ?>
				<a class="logolink" href="<?php echo esc_url( home_url( '/' ) ); // Link to the home page ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); // Title it with the blog name ?>" rel="home">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 131.91003 33.09637" class="kollektivlogo">
					<defs><style>.cls-1,.cls-2{}.cls-1,.cls-2{fill-rule:evenodd;}</style></defs><title>k25logo</title>
		    	<g id="_25" data-name="25"><path class="cls-2" d="M118.69928,17.66443H107.43011V7.48193h5.64649a1.836,1.836,0,1,0,0-3.67193h-5.65241V0h5.65241a5.64615,5.64615,0,1,1,0,11.2923h-1.83649V13.854h7.45917v3.81043"/><path class="cls-2" d="M126.26282,17.66443h-5.59076V13.854h5.59076a1.836,1.836,0,1,0,0-3.67193h-5.64508V0h10.21112V3.81h-6.40113V6.37207h1.83509a5.64618,5.64618,0,1,1,0,11.29236"/></g>
		    	<g id="kollektiv"><polyline class="cls-1" points="6.172 13.473 3.85 13.757 3.85 21.259 0 21.259 0 1.724 3.85 1.724 3.85 10.217 6.115 9.935 9.286 1.724 13.787 1.724 9.598 11.435 13.901 21.259 9.399 21.259 6.172 13.473"/><path class="cls-1" d="M24.62885,14.18146c0-2.32178-.42487-4.10541-2.51972-4.10541s-2.51965,1.78363-2.51965,4.10541c0,2.32135.42492,4.1333,2.51965,4.1333s2.51972-1.81195,2.51972-4.1333m3.85089,0c0,4.07678-1.4444,7.4176-6.37061,7.4176s-6.37054-3.34082-6.37054-7.4176c0-4.07721,1.44433-7.38971,6.37054-7.38971s6.37061,3.3125,6.37061,7.38971"/><polyline class="cls-1" points="31.507 1.441 35.301 1.441 35.301 21.259 31.507 21.259 31.507 1.441"/><polyline class="cls-1" points="39.093 1.441 42.887 1.441 42.887 21.259 39.093 21.259 39.093 1.441"/><path class="cls-1" d="M54.32373,12.93536c0-1.86853-.509-2.97247-2.29346-2.97247-1.81121,0-2.29339,1.21741-2.32165,2.97247h4.61511m3.228,5.06794.0567,2.80328a20.92841,20.92841,0,0,1-5.465.79248c-4.50128,0-6.22845-2.40656-6.22845-7.21967,0-4.6148,1.98236-7.58764,6.11529-7.58764,4.16272,0,6.03058,2.49139,6.03058,6.342l-.28277,2.63312h-8.0412c.029,1.50055.79321,2.49139,2.71838,2.49139,2.03821,0,5.0965-.25494,5.0965-.25494"/><polyline class="cls-1" points="61.174 21.259 61.174 1.441 64.967 1.441 64.967 12.539 66.128 12.284 68.931 7.131 73.178 7.131 69.525 13.813 73.376 21.259 69.101 21.259 66.213 15.908 64.967 16.191 64.967 21.259 61.174 21.259"/><path class="cls-1" d="M79.68774,10.47223v5.54956c0,1.41528.425,2.15161,1.18842,2.15161.79321,0,2.01068-.05658,2.01068-.05658l.16962,3.05768a15.83485,15.83485,0,0,1-2.74585.42456c-3.14325,0-4.41718-1.58545-4.41718-5.37939V10.47223h-1.5285V7.15973h1.5285V3.16779h3.79431V7.15973H83.0282v3.3125H79.68774"/><path class="cls-1" d="M85.60291,7.13147h3.7937V21.25934h-3.7937V7.13147m0-5.436h3.7937V5.51782h-3.7937V1.6955"/><polyline class="cls-1" points="95.596 7.131 97.918 18.173 98.399 18.173 100.777 7.131 104.628 7.131 101.23 21.259 95.058 21.259 91.66 7.131 95.596 7.131"/></g>
				</svg>
				<span class="logo-unterzeile">Fotografie</span>
				</a>
			<?php endif ?>
		<div class="hauptmenu-desktop">
			<?php
			if (has_term( 'corporate', 'filter')) {
				$location = 'corporatemenu';
			} else {
				$location = 'mainmenu';
			}
			wp_nav_menu(
				array(
					'theme_location' => $location,
					'menu_class' => 'menu simple align-right',
					'items_wrap' => '<ul id="%1$s" class="%2$s" data-magellan>%3$s</ul>'
				)
			);?>
		</div>
	  <button id="menu" class="burgermenu" data-toggle="offCanvas"><span></span></button>
	</div>
	<!--<div class="hauptmenu">
		<nav class="hauptmenu-nav">
				<?php wp_nav_menu( array( 'theme_location' => 'mainmenu' ) ); // Display the user-defined menu in Appearance > Menus ?>
		</nav>
	</div>-->
</header><!-- #masthead .site-header -->

<main class="main-fluid"><!-- start the page containter -->
