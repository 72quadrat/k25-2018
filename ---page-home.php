<?php
/**
 * 	Template Name: Home Page
 *
 *	This page template has a sidebar built into it,
 * 	and can be used as a home page, in which case the title will not show up.
 *
*/
get_header(); // This fxn gets the header.php file and renders it ?>
	<div class="startteaser-wrap">
		<?php
			// Die Variablen haben alle als Endung eine Ziffer (1 bis 4)
			// Drum zunächst eine for-Schleife
			// Die Variablen selbst werden als ${'name_der_variable'.$i} geschrieben
			// Dabei ist dann $i einfach die Zahl
			for($i=1; $i <= 4; $i++) {
				${'file'.$i};
				${'link'.$i.'_ids'} = get_field( 'link'.$i );
				${'seite'.$i} = get_field( 'seite'.$i );
				${'bild'.$i} = get_field( 'bild'.$i );
				// Wenn ein Typ-Link hinterlegt ist, diesen auch benutzen
				if (${'link'.$i.'_ids'} AND ${'bild'.$i}) {
					echo '<div class="box">';
					${'link'.$i} = get_term(${'link'.$i.'_ids'});
					${'link'.$i.'_url'} = get_term_link(${'link'.$i.'_ids'});
					echo '<a href="'.${'link'.$i.'_url'}.'"';
					${'bild'.$i} = get_field( 'bild'.$i );
					${'bild'.$i} = wp_get_attachment_image_src( ${'bild'.$i}, 'medium' );
					echo 'style="background-image:url(\''.${'bild'.$i}[0].'\')">';
					echo '<span>'.${'link'.$i}->name.'</span>';
					echo '</a>';
					echo '</div>';
				// Wenn doch kein Typ-Link da ist, vielleicht aber eine Seite?
				} elseif (${'seite'.$i} AND ${'bild'.$i}) {
					echo '<div class="box">';
					echo '<a href="'.${'seite'.$i}.'"';
					${'bild'.$i} = get_field( 'bild'.$i );
					${'bild'.$i} = wp_get_attachment_image_src( ${'bild'.$i}, 'medium' );
					echo 'style="background-image:url(\''.${'bild'.$i}[0].'\')">';
					echo '<span>'.get_the_title( url_to_postid( ${'seite'.$i} ) ).'</span>';
					echo '</a>';
					echo '</div>';
				}
			}
		?>
	</div>

	<div id="primary" style="display: none;">
		<?php
			$typen = get_terms( 'typ', array(
    		'hide_empty' => false,
			));
			// echo '<pre>';
			// print_r($typen);
			// echo '</pre>';
		?>
		<div id="content" role="main">
			<div class="startteaser-box">

				<?php
					// Die Variablen haben alle als Endung eine Ziffer (1 bis 4)
					// Drum zunächst eine for-Schleife
					// Die Variablen selbst werden als ${'name_der_variable'.$i} geschrieben
					// Dabei ist dann $i einfach die Zahl
					for($i=1; $i <= 4; $i++) {
						${'file'.$i};
						${'link'.$i.'_ids'} = get_field( 'link'.$i );
						${'seite'.$i} = get_field( 'seite'.$i );
						${'bild'.$i} = get_field( 'bild'.$i );
						// Wenn ein Typ-Link hinterlegt ist, diesen auch benutzen
						if (${'link'.$i.'_ids'} AND ${'bild'.$i}) {
							echo '<div class="box">';
							${'link'.$i} = get_term(${'link'.$i.'_ids'});
							${'link'.$i.'_url'} = get_term_link(${'link'.$i.'_ids'});
							echo '<a href="'.${'link'.$i.'_url'}.'"';
							${'bild'.$i} = get_field( 'bild'.$i );
							${'bild'.$i} = wp_get_attachment_image_src( ${'bild'.$i}, 'medium' );
							echo 'style="background-image:url(\''.${'bild'.$i}[0].'\')">';
							echo '<span>'.${'link'.$i}->name.'</span>';
							echo '</a>';
							echo '</div>';
						// Wenn doch kein Typ-Link da ist, vielleicht aber eine Seite?
						} elseif (${'seite'.$i} AND ${'bild'.$i}) {
							echo '<div class="box">';
							echo '<a href="'.${'seite'.$i}.'"';
							${'bild'.$i} = get_field( 'bild'.$i );
							${'bild'.$i} = wp_get_attachment_image_src( ${'bild'.$i}, 'medium' );
							echo 'style="background-image:url(\''.${'bild'.$i}[0].'\')">';
							echo '<span>'.get_the_title( url_to_postid( ${'seite'.$i} ) ).'</span>';
							echo '</a>';
							echo '</div>';
						}
					}
				?>

      </div>
		</div><!-- #content .site-content -->
	</div><!-- #primary .content-area -->
<?php get_footer(); // This fxn gets the footer.php file and renders it ?>
