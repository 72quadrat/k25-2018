<?php
/**
 * The template for displaying the home/index page.
 * This template will also be called in any case where the Wordpress engine
 * doesn't know which template to use (e.g. 404 error)
 */

get_header(); // This fxn gets the header.php file and renders it ?>
	<div id="primary" class="row-fluid page-portrait">
		<div id="content" role="main" class="span8 offset2">
			<!--<div class="intro"><h2><?php single_term_title(); ?>.</h2></div>-->

			<div class="filter">
				<div class="filter-intro">
					<button class="filter_toggler">
						<svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M18 18h-12c-3.311 0-6-2.689-6-6s2.689-6 6-6h12.039c3.293.021 5.961 2.701 5.961 6 0 3.311-2.688 6-6 6zm-12-10c2.208 0 4 1.792 4 4s-1.792 4-4 4-4-1.792-4-4 1.792-4 4-4z"/></svg><span>Bilder filtern</span></button>
				</div>
				<div class="filter-funktion">
					<ul class="menu menu-fotografen button-group" data-filter-group="fotografen">
						<!--<li class="menu-text">Fotografen</li>-->
						<li class="is-active"><a href="#" data-filter="*">Alle Fotografen</a></li>
						<?php
							$args = array(
								'role'         => '',
								'role__in'     => array('editor'),
								'role__not_in' => array(),
								'meta_key'     => '',
								'meta_value'   => '',
								'meta_compare' => '',
								'meta_query'   => array(),
								'date_query'   => array(),
								'include'      => array(),
								'exclude'      => array(1,2),
								'orderby'      => 'display_name',
								'order'        => 'ASC',
								'offset'       => '',
								'search'       => '',
								'number'       => '',
								'count_total'  => true,
								'fields'       => 'all',
								'who'          => '',
							);
							$fotografen = get_users($args);
							foreach ($fotografen as $fotograf) {
								echo '<li class="fotograflink-'.$fotograf->data->user_nicename.'"><a href="#" data-filter=".fotograf-'.$fotograf->ID.'">'.$fotograf->data->display_name.'</a></li>';
							}
						?>
					</ul>
					<ul class="menu menu-kategorien button-group" data-filter-group="kategorien">
						<!--<li class="menu-text">Kategorien</li>-->
						<li class="is-active"><a href="#" data-filter="*">Alle Kategorien</a></li>
						<?php
							$filter = get_terms( array(
									'taxonomy' => 'filter',
									'hide_empty' => true,
									'child_of' => get_queried_object_id(),
							) );
							foreach ($filter as $filter_element) {
								echo '<li><a href="#" data-filter=".filter-'.$filter_element->slug.'">'.$filter_element->name.'</a></li>';
							}
							?>
					</ul>
				</div>
			</div>

			<div class="images" id="fotos" data-magellan-target="fotos">
	      <div class="grid grid-x images-content">

					<?php if ( have_posts() ) :
					// Do we have any posts in the databse that match our query?
					// In the case of the home page, this will call for the most recent posts
					?>

						<?php while ( have_posts() ) : the_post();
						// If we have some posts to show, start a loop that will display each one the same way
						?>

							<!-- //////////////////////////////////////////////// -->
							<!-- ARTIKEL START -->
							<?php
								$author_class = 'fotograf-'.trim(strtolower(get_the_author_meta('first_name')));
								$author_class .= ' fotograf-'.get_the_author_meta('ID');
								if (has_term( 'corporate', 'filter')) {
									$post_classes = array('hentry',' small-6','medium-4','large-4', $author_class);
								} else {
									$post_classes = array('hentry',' small-6','medium-4','large-4', $author_class);
								}
							?>
							<article <?php post_class( $post_classes ); ?> id="<?php the_ID();?>">
								<?php get_template_part( 'loop', 'thumbnail' ); ?>
							</article>

							<!-- ARTIKEL ENDE -->
							<!-- //////////////////////////////////////////////// -->

						<?php endwhile; // OK, let's stop the posts loop once we've exhausted our query/number of posts ?>

						<!-- pagintation -->
						<div id="pagination" class="clearfix">
							<div class="past-page"><?php previous_posts_link( 'newer' ); // Display a link to  newer posts, if there are any, with the text 'newer' ?></div>
							<div class="next-page"><?php next_posts_link( 'older' ); // Display a link to  older posts, if there are any, with the text 'older' ?></div>
						</div><!-- pagination -->


					<?php else : // Well, if there are no posts to display and loop through, let's apologize to the reader (also your 404 error) ?>

						<article class="post error">
							<h1 class="404">Nothing has been posted like that yet</h1>
						</article>

					<?php endif; // OK, I think that takes care of both scenarios (having posts or not having any posts) ?>

				</div><!-- .grid -->
			</div><!-- .images -->

			<!-- //////////////////////////////////////////////// -->
			<!-- ABOUT START -->
			<div class="panel" id="about" data-magellan-target="about">
				<?php
					// ---------------------------------------------------------
					// Die Unterseite auslesen
					$current_term_slug = get_queried_object()->slug;
					$about = new WP_Query( array('pagename'=> 'footer-'.$current_term_slug));
				?>
				<?php if ( $about->have_posts() ) : while ( $about->have_posts() ) : $about->the_post(); ?>
					<article>
						<div class="about-content">
							<?php the_content(); ?>
							<?php edit_post_link('Footer bearbeiten', '<p>', '</p>'); ?>
							<button class="hollow button scrollup"><svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M23.247 20l-11.247-14.44-11.263 13.44-.737-.678 12-14.322 12 15.335-.753.665z"/></svg> nach oben</button>
						</div>
					</article>
				<?php endwhile; ?>
				<?php	wp_reset_postdata(); ?>
				<?php else : ?>
					<h2>Es gibt keine Seite mit dem Slug: "footer-<?php echo $current_term_slug ?>"</h2>
					<p>Drum wird hier auch kein About-Bereich angezeigt.</p>
				<?php endif; ?>
			</div>
			<!-- ABOUT ENDE -->
			<!-- //////////////////////////////////////////////// -->

			<!-- //////////////////////////////////////////////// -->
			<!-- KONTAKT START -->
			<div class="panel" id="kontakt" data-magellan-target="kontakt">
				<?php
					// ---------------------------------------------------------
					// Die Unterseite auslesen
					$kontakt = new WP_Query( array('pagename'=> 'kontakt'));
				?>
				<?php if ( $kontakt->have_posts() ) : while ( $kontakt->have_posts() ) : $kontakt->the_post(); ?>
					<article>
						<div class="about-content">
							<?php the_content(); ?>
							<?php edit_post_link('Kontakt bearbeiten', '<p>', '</p>'); ?>
							<button class="hollow button scrollup"><svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M23.247 20l-11.247-14.44-11.263 13.44-.737-.678 12-14.322 12 15.335-.753.665z"/></svg> nach oben</button>
						</div>
					</article>
				<?php endwhile; ?>
				<?php	wp_reset_postdata(); ?>
				<?php else : ?>
					<h2>Es gibt keine Seite mit dem Slug: "kontakt"</h2>
					<p>Drum wird hier auch nix angezeigt.</p>
				<?php endif; ?>
			</div>
			<!-- KONTAKT ENDE -->
			<!-- //////////////////////////////////////////////// -->

		</div><!-- #content .site-content -->
	</div><!-- #primary .content-area -->

	<!-- PHOTOSWIPE DOM ELEMENT -------------------------------------- -->
	<!-- Root element of PhotoSwipe. Must have class pswp. -->
	<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

			<!-- Background of PhotoSwipe.
					 It's a separate element as animating opacity is faster than rgba(). -->
			<div class="pswp__bg"></div>

			<!-- Slides wrapper with overflow:hidden. -->
			<div class="pswp__scroll-wrap">

					<!-- Container that holds slides.
							PhotoSwipe keeps only 3 of them in the DOM to save memory.
							Don't modify these 3 pswp__item elements, data is added later on. -->
					<div class="pswp__container">
							<div class="pswp__item"></div>
							<div class="pswp__item"></div>
							<div class="pswp__item"></div>
					</div>

					<!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
					<div class="pswp__ui pswp__ui--hidden">

							<div class="pswp__top-bar">

									<!--  Controls are self-explanatory. Order can be changed. -->

									<div class="pswp__counter"></div>

									<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

									<button class="pswp__button pswp__button--share" title="Share"></button>

									<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

									<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

									<!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
									<!-- element will get class pswp__preloader--active when preloader is running -->
									<div class="pswp__preloader">
											<div class="pswp__preloader__icn">
												<div class="pswp__preloader__cut">
													<div class="pswp__preloader__donut"></div>
												</div>
											</div>
									</div>
							</div>

							<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
									<div class="pswp__share-tooltip"></div>
							</div>

							<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
							</button>

							<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
							</button>

							<div class="pswp__caption">
									<div class="pswp__caption__center"></div>
							</div>

					</div>

			</div>

	</div><!-- PHOTOSWIPE DOM ELEMENT -------------------------------------- -->

<?php get_footer(); // This fxn gets the footer.php file and renders it ?>
