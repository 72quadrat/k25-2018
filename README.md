Kollektiv 25
============
Das neue Theme f�r's Kollektiv 25 basiert auf [naked](http://naked-wordpress.bckmn.com/)

### Alte QTranslate Inhalte entfernen
Wenn die Altdaten via XML Export aus Wordpress �bernommen werden sollen, kann man mit Regular Expressions die Daten bereinigen.
Eine Suche um die englischen Texte zu entfernen w�re: ```(?s)(?<=(<!--:en-->)).*?(?=(<!--:-->))```

