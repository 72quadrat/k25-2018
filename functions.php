<?php
	/*-----------------------------------------------------------------------------------*/
	/* This file will be referenced every time a template/page loads on your Wordpress site
	/* This is the place to define custom fxns and specialty code
	/*-----------------------------------------------------------------------------------*/

// Define the version so we can easily replace it throughout the theme
define( 'NAKED_VERSION', 0.4 );

/*-----------------------------------------------------------------------------------*/
/*  Set the maximum allowed width for any content in the theme
/*-----------------------------------------------------------------------------------*/
if ( ! isset( $content_width ) ) $content_width = 900;

/*-----------------------------------------------------------------------------------*/
/* Add Rss feed support to Head section
/*-----------------------------------------------------------------------------------*/
add_theme_support( 'automatic-feed-links' );

/*-----------------------------------------------------------------------------------*/
/* Add post thumbnail/featured image support
/*-----------------------------------------------------------------------------------*/
add_theme_support( 'post-thumbnails' );

/*-----------------------------------------------------------------------------------*/
/* Add post thumbnail/featured image support
/*-----------------------------------------------------------------------------------*/
add_theme_support( 'post-formats', array( 'aside', 'image' ) );


/*-----------------------------------------------------------------------------------*/
/* Register main menu for Wordpress use
/*-----------------------------------------------------------------------------------*/
register_nav_menus(
	array(
		'primary'	=>	__( 'Primary Menu', 'naked' ), // Register the Primary menu
		'footer'	=>	__( 'Footer Menu', 'naked' ), // Register the Primary menu
		// just change the 'primary' to another name
	)
);

/*-----------------------------------------------------------------------------------*/
/* Activate sidebar for Wordpress use
/*-----------------------------------------------------------------------------------*/
function naked_register_sidebars() {
	register_sidebar(array(				// Start a series of sidebars to register
		'id' => 'sidebar', 					// Make an ID
		'name' => 'Sidebar',				// Name it
		'description' => 'Take it on the side...', // Dumb description for the admin side
		'before_widget' => '<div>',	// What to display before each widget
		'after_widget' => '</div>',	// What to display following each widget
		'before_title' => '<h3 class="side-title">',	// What to display before each widget's title
		'after_title' => '</h3>',		// What to display following each widget's title
		'empty_title'=> '',					// What to display in the case of no title defined for a widget
		// Copy and paste the lines above right here if you want to make another sidebar,
		// just change the values of id and name to another word/name
	));
}
// adding sidebars to Wordpress (these are created in functions.php)
add_action( 'widgets_init', 'naked_register_sidebars' );

/*-----------------------------------------------------------------------------------*/
/* Activate Menu
/*-----------------------------------------------------------------------------------*/
function register_menu() {

	$locations = array(
		'mainmenu' => __( 'Das Hauptmenu', 'naked' ),
		'corporatemenu' => __( 'Das Hauptmenu für den Corporate-Bereich', 'naked' ),
		'footermenu' => __( 'Das Menu im Footer', 'naked' ),
	);
	register_nav_menus( $locations );

}
add_action( 'init', 'register_menu' );
/*-----------------------------------------------------------------------------------*/
/* Enqueue Styles and Scripts
/*-----------------------------------------------------------------------------------*/

function naked_scripts()  {

	// get the theme directory style.css and link to it in the header
	wp_enqueue_style('webfonts','https://fonts.googleapis.com/css?family=Titillium+Web:200,300,400,400i,700,900');
	wp_enqueue_style('style.css', get_stylesheet_directory_uri() . '/css/app.css');

	// add theme scripts
	wp_enqueue_script( 'plugins', get_template_directory_uri() . '/js/plugins-min.js', array( 'jquery' ), NAKED_VERSION, true );
	wp_enqueue_script( 'app', get_template_directory_uri() . '/js/app-min.js', array( 'jquery' ), NAKED_VERSION, true );

}
add_action( 'wp_enqueue_scripts', 'naked_scripts' ); // Register this fxn and allow Wordpress to call it automatcally in the header

/*-----------------------------------------------------------------------------------*/
/* Bildgröße hinterlegen
/*-----------------------------------------------------------------------------------*/

add_image_size( 'xs', 160, 160 );
add_image_size( 's',  320, 320 );
add_image_size( 'm',  640, 640 );
add_image_size( 'l',  960, 960 );
add_image_size( 'xl', 1280, 1280 );
add_image_size( 'xxl', 1600, 1600 );
add_image_size( 'xxxl', 1920, 1920 );

/*-----------------------------------------------------------------------------------*/
/* Post Typ registrieren
/*-----------------------------------------------------------------------------------*/

function k25_register_my_cpts() {

	$labels = array(
		"name" => __( "Arbeiten", "" ),
		"singular_name" => __( "Arbeit", "" ),
	);

	$args = array(
		"label" => __( "Arbeiten", "" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"has_archive" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "page",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "work", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail", "author", "page-attributes", "excerpt" ),
	);

	register_post_type( "arbeit", $args );
}
add_action( 'init', 'k25_register_my_cpts' );

/*-----------------------------------------------------------------------------------*/
/* Post Taxonomy registrieren
/*-----------------------------------------------------------------------------------*/

function k25_register_my_taxes() {

	$labels = array(
		"name" => __( "Filter", "" ),
		"singular_name" => __( "Filter", "" ),
	);

	$args = array(
		"label" => __( "Filter", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Filter",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'filter', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "filter",
		"show_in_quick_edit" => true,
	);
	register_taxonomy( "filter", array( "arbeit" ), $args );
}
add_action( 'init', 'k25_register_my_taxes' );

/*-----------------------------------------------------------------------------------*/
/* Startseite anpassen
/*-----------------------------------------------------------------------------------*/
function custom_front_page($wp_query){
    //Ensure this filter isn't applied to the admin area
    if(is_admin()) {
      return;
    }

    if($wp_query->get('page_id') == get_option('page_on_front')):

			$tax_query = array(
				'taxonomy' => 'filter',
				'field'    => 'slug',
				'terms'    => 'editorial',
				'operator' => 'IN',
			);

      $wp_query->set('post_type', 'Arbeit');
			$wp_query->set('tax_query', $tax_query);
      $wp_query->set('page_id', ''); //Empty

      //Set properties that describe the page to reflect that
      //we aren't really displaying a static page
      $wp_query->is_page = 0;
      $wp_query->is_singular = 0;
      $wp_query->is_post_type_archive = 1;
      $wp_query->is_archive = 1;

    endif;

}
add_action("pre_get_posts", "custom_front_page");

/*-----------------------------------------------------------------------------------*/
/* Thumbnails in der Übersicht ausgeben
/*-----------------------------------------------------------------------------------*/
add_filter('manage_posts_columns', 'posts_columns', 5);
add_action('manage_posts_custom_column', 'posts_custom_columns', 5, 3);
function posts_columns($defaults){
    $defaults['riv_post_thumbs_1234'] = __('Thumbnail');
    return $defaults;
}
function posts_custom_columns($column_name, $id){
   if($column_name === 'riv_post_thumbs_1234'){
        echo the_post_thumbnail( array('100, 100') );
    }
}
